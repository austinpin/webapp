hello

Asset creadits:

Space skybox avaliable at:
http://wwwtyro.github.io/space-3d/#animationSpeed=1&fov=80&nebulae=true&pointStars=true&resolution=1024&seed=44mmr1b6pd40&stars=true&sun=true

Controls:
wasd - rotate ship
space - increase speed
shift - decrease speed
i - interact (when alerted)
o - take picture

there is a bug with the show picture feature
it will only show the last downloaded picture for each picture board.
this bug originates from getting the boards,
each string is unique (so a differnt picture) however it seems to only create the same picture in the storage array.
i am certain in correct but it feels like some strange bug

The web folder is just something unity creates
the web app is the project
the SpaceFrontierMinerServer is the server
and data is stored inside FBX and textures, (this is models and posters psd files) the final files are in the game project

To play:
find an empty spot and write some text then create the board
to create with a picture, first take a picture, then press link to open the extra screen, then press picture, then create the board (dont forget to write text)

to view board, go up to a board, and it will automatically display the text,
to view an image for applicable board, press i when notified
(due to the bug, only the last uploaded image to the server will be shown)

this feature can be extended to attach items and more