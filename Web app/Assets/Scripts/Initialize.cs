﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Rendering;
using Unity.Transforms;
using UnityEngine;

//originaly designed to create the entities
//this script now only holds a referance to the data.

public class Initialize : MonoBehaviour
{
    public static Data data;

    // Start is called before the first frame update
    void Start()
    {
        data = GetComponent<Data>();
    }

    void Update()
    {
    }
}
