﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Unity.Entities;

//the data same as the server
[System.Serializable]
public struct Board
{
    public int boardID;

    public float xValue;

    public float yValue;

    public float zValue;

    public string message;

    public string dataObject;
    public AttachmentMode objectType;
}

//entity data, when the board is created is will have this data
[System.Serializable]
public struct _Board : IComponentData
{
    public int boardID;
    public AttachmentMode objectType;
}

//message data for storing inside an array and retriving later
[System.Serializable]
public struct Message
{
    public int boardID;

    public string text;
}

//picture data for storing inside array and retriving later
[System.Serializable]
public struct Picture
{
    public int boardID;

    public Texture2D picture;
}
