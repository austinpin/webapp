﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Collections;

using System.Net;
using System.IO;
using System;
using System.Text;

//from
//https://stackoverflow.com/questions/36239705/serialize-and-deserialize-json-and-json-array-in-unity
//nessesary for transfering arrays
public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}

public class GetBoards : MonoBehaviour
{

    EntityManager em;

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
        StartCoroutine(_GetBoards());
        //StartCoroutine(GetMessages());
    }

    IEnumerator _GetBoards()
    {
        //send web request
        UnityWebRequest www = UnityWebRequest.Get(Initialize.data.hostIP+"api/Board");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            string jsonResponse = www.downloadHandler.text; //get data
            jsonResponse = "{\"Items\":" + jsonResponse + "}"; //convert to array data
            Board[] boards = JsonHelper.FromJson<Board>(jsonResponse); //convert string to boards

            //create board entities
            int size = boards.Length;
            NativeArray<Entity> boardsE = new NativeArray<Entity>(size, Allocator.Temp);
            em.CreateEntity(Data.boardArchetype, boardsE);

            //give the board entities their data
            for (int i = 0; i < size; i++)
            {
                Position pos = new Position { Value = new float3(boards[i].xValue, boards[i].yValue, boards[i].zValue) };
                em.SetComponentData(boardsE[i], pos);
                em.SetComponentData(boardsE[i], new _Board { boardID = boards[i].boardID , objectType=boards[i].objectType });
                em.SetSharedComponentData(boardsE[i], new MeshInstanceRenderer { mesh = Initialize.data.boardMesh, material = Initialize.data.boardMaterial });

                //add boards message to storage in data (ECS restrictions due to string and similar types)
                Data.allMessage.Add(new Message {boardID=boards[i].boardID,text=boards[i].message });

                //add picture to list
                if (boards[i].objectType==AttachmentMode.Picture) {
                    Byte[] bytes = Convert.FromBase64String(boards[i].dataObject);
                    Texture2D newPicture= Texture2D.blackTexture;
                    newPicture.LoadImage(bytes);
                    Data.allPicture.Add(new Picture { boardID = boards[i].boardID, picture = newPicture});
                }
            }
        }
    }

    /*
    IEnumerator GetMessages()
    {
        //unused
        UnityWebRequest www = UnityWebRequest.Get(Initialize.data.hostIP +"api/Messages");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            //Debug.Log(www.downloadHandler.text);

            //convert string to board data
            string jsonResponse = www.downloadHandler.text;
            jsonResponse = "{\"Items\":" + jsonResponse + "}";
            Message[] messages = JsonHelper.FromJson<Message>(jsonResponse);

            Data.allMessage.AddRange(messages);
        }
    }
    */

    // Update is called once per frame
    void Update()
    {

    }
}
