﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using Unity.Mathematics;

public class SendComment : MonoBehaviour
{
    public Text inputField;
    EntityManager em;

    //this works
    //many thanks 
    //https://stackoverflow.com/questions/46003824/sending-http-requests-in-c-sharp-with-unity

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnPress()
    {
        if (Data.buttonMode == ButtonMode.Send)
        {
            //StartCoroutine(Upload());
        }
        else if (Data.buttonMode == ButtonMode.Create)
        {
            StartCoroutine(Create());
        }
    }

    /*
    IEnumerator Upload()
    {

        string message = inputField.text;
        Message m = new Message { text = message, boardID = MessageSystem.currentBoardID };
        string post = JsonUtility.ToJson(m);


        var uwr = new UnityWebRequest(Initialize.data.hostIP +"api/Messages", "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(post);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");
        //

        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.Log(uwr.error);
        }
        else
        {
            Debug.Log("Form upload complete!");
        }
    }
    */

    IEnumerator Create()
    {
        //create board at player position
        float3 playerPos = new float3(Data.playerPos.x,Data.playerPos.y,Data.playerPos.z) ;
        Board b = new Board { xValue = playerPos.x, yValue = playerPos.y, zValue = playerPos.z , message = inputField.text };

        //if we are attaching picture, convert screenshot in data to string and append to board b
        if (Data.attachmentMode==AttachmentMode.Picture)
        {
            string picture;

            byte[] bytes = Initialize.data.screenShot.EncodeToJPG();
            picture = Convert.ToBase64String(bytes);

            b.dataObject = picture;
            b.objectType = AttachmentMode.Picture;

            //irrelavent for upload, but put picture back into data
            byte[] newbyte = Convert.FromBase64String(picture);
            Texture2D newtexture = Texture2D.blackTexture;
            ImageConversion.LoadImage(newtexture, newbyte);
            Initialize.data.downloadPicture = newtexture;
        }

        string post = JsonUtility.ToJson(b);

        //send to server
        var uwr = new UnityWebRequest(Initialize.data.hostIP + "api/Board", "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(post);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        yield return uwr.SendWebRequest();

        if (uwr.isNetworkError || uwr.isHttpError)
        {
            Debug.Log(uwr.error);
        }
        else
        {
            //if it works create the board
            Entity board = em.CreateEntity(Data.boardArchetype);
            Position pos = new Position { Value = playerPos };
            em.SetComponentData(board, pos);
            em.SetComponentData(board, new _Board {  });
            em.SetSharedComponentData(board, new MeshInstanceRenderer { mesh = Initialize.data.boardMesh, material = Initialize.data.boardMaterial });

            //add boards message to storage in data (ECS restrictions due to string and similar types)
            Data.allMessage.Add(new Message { text = b.message });
        }
    }
}
