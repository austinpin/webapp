﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class MessageSystem : ComponentSystem
{

    //inject entity data
    struct player
    {
        public ComponentDataArray<Position> pos;
        public ComponentDataArray<PlayerTag> tag;
    }

    struct boards
    {
        public readonly int Length;
        public ComponentDataArray<Position> pos;
        public ComponentDataArray<_Board> board;
    }

    [Inject] player _player;
    [Inject] boards _boards;

    public static int currentBoardID;

    protected override void OnUpdate()
    {
        int closestIndex = -1;
        float closestDistance = 40;
        AttachmentMode closestAttach;

        //find closest board
        for (int i = 0; i < _boards.Length; i++)
        {
            float distance = math.distance(_player.pos[0].Value, _boards.pos[i].Value);
            if ( distance < closestDistance)
            {
                closestIndex = i;
                closestDistance = distance;
                closestAttach = _boards.board[i].objectType;
            }
        }
        if (closestIndex!=-1) {
            //if we are close to a board and its a different board
            if (currentBoardID != _boards.board[closestIndex].boardID)
            {
                //remove old
                DestroyCommentObjects();
                Data.createaButtonText.text = "";
                Data.buttonMode = ButtonMode.Send;

                currentBoardID = _boards.board[closestIndex].boardID;

                //create comment object and give data
                int height = 100;
                foreach (Message m in Data.allMessage)
                {
                    if (m.boardID==currentBoardID)
                    {
                        GameObject g = GameObject.Instantiate(Initialize.data.messageObject, Data.content) as GameObject;
                        Data.commentPosts.Add(g);
                        RectTransform r = g.GetComponent<RectTransform>();
                        r.anchoredPosition = new Vector3(-8, height, 0);
                        height += 100;
                        g.transform.GetChild(0).GetComponent<Text>().text = m.text;
                    }
                }

                //assign picture to data-download for viewing manually
                foreach(Picture p in Data.allPicture)
                {
                    if (p.boardID==currentBoardID)
                    {
                        Initialize.data.downloadPicture = p.picture;
                        Initialize.data.showPicture.texture = Initialize.data.downloadPicture;
                        Initialize.data.showPicture.SetNativeSize();
                        //Initialize.data.pictureScreen.SetActive(true);
                        Initialize.data.infoText.text = "picture avaliable, press i";
                        break;
                    }
                }
            }
        }
        else
        {
            //if we are not close to any board
            if (currentBoardID!=-1) {
                DestroyCommentObjects();
                currentBoardID = -1;
                Data.buttonMode = ButtonMode.Create;
                Data.createaButtonText.text = "Create";
                Initialize.data.infoText.text = "";
                //Initialize.data.downloadPicture = null;
            }
        }
    }

    
    void DestroyCommentObjects()
    {
        foreach (GameObject g in Data.commentPosts)
        {

            GameObject.Destroy(g);
        }
        Data.commentPosts.Clear();
    }
}
