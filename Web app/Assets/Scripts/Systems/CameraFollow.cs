﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

//unused now 

public class CamBarrier : BarrierSystem { }

[UpdateAfter(typeof(PlayerInputSystem))]
public class CameraFollow : JobComponentSystem
{
    public static JobHandle cameraHandle;

    /*
    bool once = true;

    struct player
    {
        public EntityArray self;
        [ReadOnly] public ComponentDataArray<PlayerInput> input;
        [ReadOnly] public ComponentDataArray<Rotation> rotation;
        [ReadOnly] public ComponentDataArray<Position> position;
    }

    struct CameraCenters
    {
        public EntityArray self;
        public ComponentDataArray<CameraCentre> centre;
    }

    struct CameraTags
    {
        public readonly int Length;
        public EntityArray self;
        public ComponentDataArray<CameraTag> tag;
        public ComponentDataArray<Position> position;
    }

    [Inject] player _player;
    [Inject] CameraCenters _cameraCentres;
    [Inject] CameraTags _cameraTags;
    [Inject] CamBarrier buffer;


    
    [BurstCompile]
    struct GetTargetPosJob : IJobProcessComponentData<CameraCentre>
    {
        [NativeDisableParallelForRestriction][ReadOnly]
        public ComponentDataArray<Rotation> rotation;
        [NativeDisableParallelForRestriction]
        [ReadOnly]
        public ComponentDataArray<Position> position;

        public float left;
        public float lookUp;

        public void Execute(ref CameraCentre centre)
        {

            centre.targetPos = position[0].Value;
            centre.targetRot = rotation[0].Value;

            if (centre.id==1)
            {
                centre.lookHorizontal = left * -1;
            }
            else if (centre.id==2)
            {
                centre.lookVertical = lookUp * -1;
            }


        }
    }

    [BurstCompile]
    struct MoveCameraJob : IJobProcessComponentData<CameraCentre,Position,Rotation>
    {


        public void Execute([ReadOnly] ref CameraCentre centre, [ReadOnly] ref Position pos, ref Rotation rot)
        {
            /*
            quaternion left = quaternion.RotateY(-centre.lookHorizontal * 0.025f);
            quaternion right = quaternion.RotateZ(centre.lookVertical * 0.025f);
            quaternion final = math.mul(math.normalize(left), math.normalize(right));
            rot.Value = math.mul(rot.Value, math.normalize(final));
            //

            if (centre.id == 1)
            {
                quaternion left = quaternion.RotateY(-centre.lookHorizontal * 0.025f);
                rot.Value = math.mul(rot.Value, math.normalize(left));
            }
            else if (centre.id == 2)
            {
                quaternion right = quaternion.RotateX(centre.lookVertical * 0.025f);
                rot.Value = math.mul(rot.Value, math.normalize(right));
            }
        }
    }

    [BurstCompile]
    struct SetTargetPosJob : IJobProcessComponentData<CameraCentre,Position,Rotation>
    {

        public void Execute([ReadOnly]ref CameraCentre cam, ref Position pos, ref Rotation rot)
        {
            pos.Value = cam.targetPos;
            //rot.Value = cam.targetRot;
        }
    }
    */

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        /*
        if (once)
        {

            once = false;
        }

        GetTargetPosJob getTargetPosJob = new GetTargetPosJob
        {
            position=_player.position,
            rotation=_player.rotation,
            left = Input.GetAxis("Mouse X"),
            lookUp = Input.GetAxis("Mouse Y"),
        };
        JobHandle newHandle = getTargetPosJob.Schedule(this, inputDeps);

        MoveCameraJob moveCameraJob = new MoveCameraJob
        { };
        JobHandle newHandle2 = moveCameraJob.Schedule(this, newHandle);

        SetTargetPosJob setTargetPosJob = new SetTargetPosJob
        {
        };
        cameraHandle = setTargetPosJob.Schedule(this, newHandle2);
        */

        return cameraHandle;
    }
}
