﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

//unused
//as player movement has been moved to monobehaviour for easier use

public class PlayerInputSystem : JobComponentSystem
{
    public static JobHandle playerInputhandle;

    struct player
    {
        public ComponentDataArray<Position> pos;
        public ComponentDataArray<PlayerTag> tag;
    }

    [Inject] player _player;

    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {

        Position pos = _player.pos[0];
        pos.Value = Initialize.data.player.position;
        _player.pos[0] = pos;

        return playerInputhandle;
    }
}
