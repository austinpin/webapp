﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Rendering;
using UnityEngine.UI;

public class Data : MonoBehaviour
{

    public EntityManager em;

    public Transform player;
    public static Vector3 playerPos;

    [Header("board data")]
    public Mesh boardMesh;
    public Material boardMaterial;

    public string hostIP;

    [Header("screens")]
    public GameObject pictureScreen;
    public GameObject attachmentsScreen;
    public GameObject ScrollScreen;
    public Text infoText;

    [Header("picture data")]
    public Texture2D screenShot;
    public Texture2D downloadPicture;
    public RawImage showPicture;

    //the comment object with text inside for creation
    public GameObject messageObject;

    //where the comments are put
    public static Transform content;
    //the button is the create button below the scroll rect
    public static Text createaButtonText;

    //the board entity "prefab"
    public static EntityArchetype boardArchetype;

    //the different modes
    //[Header("modes")]
    public static ButtonMode buttonMode;
    public static AttachmentMode attachmentMode;
    //the feedback displayed atop the attachment screen
    public Text attachFeedback;

    //list of all messages
    public static List<Message> allMessage = new List<Message>();
    //all of the pictures downloaded
    public static List<Picture> allPicture = new List<Picture>();

    //list of commnet gameobjects to manage showing and hiding(deleting) comments
    public static List<GameObject> commentPosts = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        em = World.Active.GetOrCreateManager<EntityManager>();

        infoText.text = "";

        content = GameObject.Find("Content").transform;
        createaButtonText = GameObject.Find("Send Button Text").GetComponent<Text>();

        boardArchetype = em.CreateArchetype(
            ComponentType.Create<Position>(),
            ComponentType.Create<_Board>(),
            ComponentType.Create<MeshInstanceRenderer>()
            );
    }

    // Update is called once per frame
    void Update()
    {
    }
}
