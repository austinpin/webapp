﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//controled by buttons

public class SetAttachMode : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AttachModeNone()
    {
        Data.attachmentMode = AttachmentMode.None;
        Initialize.data.attachFeedback.text = "Nothing attached";
    }

    public void AttachModePicture()
    {
        if (Initialize.data.screenShot !=null) {
            Data.attachmentMode = AttachmentMode.Picture;
            Initialize.data.attachFeedback.text = "picture attached";
        }
        else
        {
            Initialize.data.attachFeedback.text = "take a picture first with o";
        }
    }
}
