﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//rotates camera

public class CameraController : MonoBehaviour
{
    public Transform parent;
    public Transform me;
    public Transform cam;

    private float xAxis;
    private float yAxis;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        xAxis -= Input.GetAxis("Mouse Y");
        yAxis += Input.GetAxis("Mouse X");

        parent.eulerAngles = new Vector3(0,yAxis,0);
        me.localEulerAngles = new Vector3(0,0,xAxis);

        //cam.LookAt(parent);
    }
}
