﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//controlled by button
//shows screen

public class LinkButton : MonoBehaviour
{

    public static bool showAttachmentsScreen=false;

    // Start is called before the first frame update
    void Start()
    {
        Initialize.data.attachmentsScreen.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowAttachmentsScreen()
    {
        showAttachmentsScreen = !showAttachmentsScreen;

        if (showAttachmentsScreen)
        {
            Initialize.data.attachmentsScreen.SetActive(true);
        }
        else
        {
            Initialize.data.attachmentsScreen.SetActive(false);
        }
    }
}
