﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class PlayerInputMono : MonoBehaviour
{

    public Transform self;
    public Transform child;

    //movement in direction
    public float xAxis;
    public float yAxis;

    public float maxSpeed;
    public float currentSpeed;

    //entity component
    //this gameobject is still an entity to interact with boards
    Position pos;

    bool ShowDownloadPicture=false;

    // Start is called before the first frame update
    void Start()
    {
        //grab data (notice .value at end)
        pos = GetComponent<PositionComponent>().Value;
    }

    // Update is called once per frame
    void Update()
    {
        xAxis += Input.GetAxis("Horizontal");
        yAxis -= Input.GetAxis("Vertical");
        currentSpeed += Input.GetAxis("Move Up");

        if (currentSpeed >= maxSpeed)
        {
            currentSpeed = maxSpeed;
        } else if (currentSpeed <= 0)
        {
            currentSpeed = 0;
        }

        self.localEulerAngles = new Vector3(0, xAxis, 0);
        child.localEulerAngles = new Vector3(yAxis, 0, 0);

        self.position += currentSpeed * child.forward * Time.deltaTime;

        //write position to entity component too, and global data access
        pos.Value = self.position;
        Data.playerPos = self.position;

        if (Input.GetButtonDown("ScreenShot"))
        {
            //dont take screenshot of screenshot
            if (!Initialize.data.pictureScreen.activeInHierarchy) {
                StartCoroutine(ScreenShot());
            }
        }
        if (Input.GetButtonDown("Interact"))
        {
            ShowDownloadPicture = !ShowDownloadPicture;
            if (ShowDownloadPicture)
            {
                Initialize.data.showPicture.texture = Initialize.data.downloadPicture;
                Initialize.data.showPicture.SetNativeSize();
                Initialize.data.pictureScreen.SetActive(true);
            }
            else
            {
                Initialize.data.pictureScreen.SetActive(false);
            }
        }
    }

    IEnumerator ScreenShot()
    {
        //disable ui, or it will appear in screenshot
        Initialize.data.attachmentsScreen.SetActive(false) ;
        Initialize.data.ScrollScreen.SetActive(false);
        LinkButton.showAttachmentsScreen = false;

        //code primarily from unity scriting api
        yield return new WaitForEndOfFrame();

        int width = Screen.width;
        int height = Screen.height;
        Initialize.data.screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);

        //read screen buffer
        Initialize.data.screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        Initialize.data.screenShot.Apply();

        //assign screenshot to data (for later use), and show to player
        Initialize.data.showPicture.texture = Initialize.data.screenShot;
        Initialize.data.showPicture.SetNativeSize();
        Initialize.data.pictureScreen.SetActive(true);
    }
}
