﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//controlled by button
//set screenshot false and scroll screen active -  after screenshot

public class HidePicture : MonoBehaviour
{

    public GameObject picture;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    
    public void HidePictureScreen()
    {
        picture.SetActive(false);
        Initialize.data.ScrollScreen.SetActive(true);
    }
}
