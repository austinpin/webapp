﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//just the different modes

public enum ButtonMode
{
    Send,
    Create
}

public enum AttachmentMode
{
    None,
    Picture
}
