﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;


[System.Serializable]
public struct CameraCentre : IComponentData
{
    public Entity target;
    public float3 targetPos;
    public quaternion targetRot;
    public float3 offset;

    public byte id;

    public float lookHorizontal;
    public float lookVertical;
}
public class CameraCentreComponent : ComponentDataWrapper<CameraCentre> { };