﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct PlayerInput : IComponentData
{
    public float speed;
    public float maxSpeed;

    public float up;
    public float foward;
    public float strafe;

    public byte ID;
    public float rotX;
    public float rotY;
}
public class PlayerInputComponent : ComponentDataWrapper<PlayerInput> { };