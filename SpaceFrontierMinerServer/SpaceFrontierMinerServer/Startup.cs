﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using SpaceFrontierMinerServer.Data;
using Microsoft.EntityFrameworkCore;

delegate void sql(string p);

namespace SpaceFrontierMinerServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connection = Configuration.GetConnectionString("DefaultConnection");
            /*
            DbContextOptionsBuilder a = new DbContextOptionsBuilder();


            Action<DbContextOptionsBuilder> b = new Action<DbContextOptionsBuilder>(a.UseSqlServer);
            b.UseSqlServer(connection);
            */
            services.AddDbContext<CommentContext>(opt => opt.UseSqlServer(connection));
            //services.AddDbContext<CommentContext>(b);
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, CommentContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();

            DBStart.Initialize(context);
        }
    }
}
