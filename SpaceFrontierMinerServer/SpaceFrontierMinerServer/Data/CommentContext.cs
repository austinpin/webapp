﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using SpaceFrontierMinerServer.Models;

//messages redundant now

namespace SpaceFrontierMinerServer.Data
{
    public class CommentContext :DbContext
    {
        public CommentContext(DbContextOptions<CommentContext> options): base(options)
        {

        }

        public DbSet<Message> Messages { get; set; }
        public DbSet<Board> Boards { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>().ToTable("Message");
            modelBuilder.Entity<Board>().ToTable("Board");
        }
    }
}