﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpaceFrontierMinerServer.Data;
using SpaceFrontierMinerServer.Models;

namespace SpaceFrontierMinerServer.Data
{
    public class DBStart
    {
        public static void Initialize(CommentContext context)
        {
            context.Database.EnsureCreated();

            if(context.Boards.Any())
            {
                return;
            }

            //resolve this as well
            Board[] _b = new Board[3];

            _b[0] = new Board { xValue = 5, yValue = 5, zValue = 5 , message = "hello" };
            _b[1] = new Board { xValue = 25, yValue = 25, zValue = 25 , message = "and" };
            _b[2] = new Board { xValue = -25, yValue = -25, zValue = -25 , message = "welcome" };

            foreach (Board b in _b)
            {
                context.Boards.Add(b);
            }
            context.SaveChanges();


            //messages redundant
            /*
            Message[] messages = new Message[6];

            messages[0] = new Message {text="hello",boardID=1 };
            messages[1] = new Message {text="welcome",boardID=1 };
            messages[2] = new Message {text="to",boardID=2 };
            messages[3] = new Message {text="space",boardID=2 };
            messages[4] = new Message {text="frotier",boardID=3 };
            messages[5] = new Message {text="miner",boardID=3 };

            foreach(Message m in messages)
            {
                context.Messages.Add(m);
            }
            context.SaveChanges();
            */
        }
    }
}
