﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SpaceFrontierMinerServer.Models
{
    public class Board
    {
        public int boardID
        {
            get;
            set;
        }

        public float xValue
        {
            get;
            set;
        }

        public float yValue
        {
            get;
            set;
        }

        public float zValue
        {
            get;
            set;
        }

        public string message
        {
            get;
            set;
        }

        public string dataObject
        {
            get;
            set;
        }

        public AttachmentMode objectType
        {
            get;
            set;
        }
    }
}