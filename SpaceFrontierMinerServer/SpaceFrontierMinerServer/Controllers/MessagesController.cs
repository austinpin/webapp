﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpaceFrontierMinerServer.Data;
using SpaceFrontierMinerServer.Models;

//unused now

namespace SpaceFrontierMinerServer.Controllers
{
    [Route("api/[controller]")]
    public class MessagesController : Controller
    {
        CommentContext context;

        public MessagesController(CommentContext _Context)
        {
            context = _Context;
        }

        // GET api/messages
        [HttpGet("{id}")]
        public Message[] Messages(int id)
        {
            List<Message> messages = new List<Message>();
            Message[] DB = context.Messages.ToArray();

            foreach(Message m in DB)
            {
                if (m.boardID==id)
                {
                    messages.Add(m);
                }
            }
            Message[] myMessage = messages.ToArray();
            return myMessage;
        }

        [HttpGet]
        public Message[] AllMessages()
        {
            Message[] messages = context.Messages.ToArray();
            return messages;
        }

        /*
        // GET api/messages/5
        [HttpGet("{id}")]
        public IEnumerable<Message> Get(int id)
        {
            Message[] allMessage = context.Messages.ToArray();
            List<Message> myMessage = new List<Message>();

            foreach (Message m in allMessage)
            {
                if (m.BoardID==id)
                {
                    myMessage.Add(m);
                }
            }
            Message[] thisMessage = myMessage.ToArray();
            return thisMessage;
        }
        */

            // POST api/messages
        [HttpPost]
        public void Post([FromBody]Message value)
        {
            context.Messages.Add(value);
            context.SaveChanges();
        }

        // PUT api/messages/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/messages/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
