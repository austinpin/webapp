﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SpaceFrontierMinerServer.Data;
using SpaceFrontierMinerServer.Models;

namespace SpaceFrontierMinerServer.Controllers
{
    [Route("api/[controller]")]
    public class BoardController : Controller
    {

        CommentContext context;

        public BoardController(CommentContext _Context)
        {
            context = _Context;
        }

        [HttpGet]
        //GET api/messsages/GetBoard
        public Board[] Board()
        {

            
            Board[] boards = context.Boards.ToArray();
            return boards;
        }


        /*
        // GET api/messages/5
        [HttpGet("{id}")]
        public IEnumerable<Message> Get(int id)
        {
            Message[] allMessage = context.Messages.ToArray();
            List<Message> myMessage = new List<Message>();

            foreach (Message m in allMessage)
            {
                if (m.BoardID==id)
                {
                    myMessage.Add(m);
                }
            }
            Message[] thisMessage = myMessage.ToArray();
            return thisMessage;
        }
        */

        // POST api/messages
        [HttpPost]
        public void Post([FromBody]Board value)
        {
            context.Boards.Add(value);
            context.SaveChanges();
        }

        // PUT api/messages/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/messages/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
